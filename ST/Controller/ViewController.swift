//
//  ViewController.swift
//  ST
//
//  Created by Mac on 2020/9/2.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var tableView:UITableView?
    var dataArr = [CollectionItemModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.title = "首页"
        self.view.backgroundColor = UIColor.green
        
        tableView = UITableView.init(frame: CGRect(x: 0, y: 88, width: self.view.bounds.size.width, height: self.view.bounds.size.height-88), style: .grouped)
        tableView?.delegate = self
        tableView?.dataSource = self
        self.view .addSubview(tableView!)
        
        tableView?.rowHeight = UITableView.automaticDimension
        tableView?.estimatedRowHeight = 200;
        
        for i in 0...8 {
            let model = CollectionItemModel()
            model.title = "item标题"+String(i)
            dataArr.append(model)
        }
        
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return UIScreen.main.bounds.size.width/4*2
        }
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        if indexPath.section == 0 {
            let cell = TableCollectionViewCell.init(style: .default, reuseIdentifier: "cell0")
            cell.dataArr = dataArr
            return cell
            
        }
        let cell = TableViewCell.init(style: .default, reuseIdentifier: "cell")
        
        cell.detailLabel?.text = "indexPath.row->"+String(indexPath.row)+"百慕大群岛（英语：Bermuda），港台译百慕达群岛，旧称萨默斯岛。位于北大西洋，是自治的英国海外领地。位于北纬32度14分至32度25分、西经64度38分至64度53分。距北美洲约900多公里、美国东岸佛罗里达州迈阿密东北约1100海里及加拿大新斯科舍省哈利法克斯东南约840海里 [1-2]。"
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 1 {
            self.navigationController?.pushViewController(NextController(), animated: true)
        }
    }
    
    

}



