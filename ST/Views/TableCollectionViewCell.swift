//
//  TableCollectionViewCell.swift
//  ST
//
//  Created by Mac on 2020/9/3.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class TableCollectionViewCell: UITableViewCell, UICollectionViewDelegate,UICollectionViewDataSource {
    
    var dataArr = [CollectionItemModel]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    var collectionView:UICollectionView?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let width = (UIScreen.main.bounds.size.width-5) / 4
        
        let layout = UICollectionViewFlowLayout.init()
        layout.itemSize = CGSize(width: width , height: width)
        layout.minimumLineSpacing = 1;
        layout.minimumInteritemSpacing = 1
        
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView?.backgroundColor = UIColor.white
        collectionView?.delegate = self
        collectionView?.dataSource = self
        self.addSubview(collectionView!)
        collectionView?.snp.makeConstraints({ (make) in
            make.edges.equalTo(self)
        })
        
        collectionView?.register(ItemCollectionViewCell.self, forCellWithReuseIdentifier: "coCellID")
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Int(dataArr.count)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:ItemCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "coCellID", for: indexPath) as! ItemCollectionViewCell
        cell.titleLabel?.text = dataArr[indexPath.row].title
        return cell
        
    }
    
}
