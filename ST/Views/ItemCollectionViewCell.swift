//
//  ItemCollectionViewCell.swift
//  ST
//
//  Created by Mac on 2020/9/2.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class ItemCollectionViewCell: UICollectionViewCell {
    
    var titleLabel:UILabel?
    var imgView:UIImageView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.white
        
        imgView = UIImageView(frame: .zero)
        imgView?.isUserInteractionEnabled = true
        self.addSubview(imgView!)
        imgView?.snp.makeConstraints({ (make) in
            make.height.width.equalTo(60)
            make.centerY.equalTo(self).offset(-10)
            make.centerX.equalTo(self)
        })
        
        let red = arc4random()%255
        let green = arc4random()%255
        let blue = arc4random()%255
        
        imgView?.layer.cornerRadius = 30
        imgView?.layer.masksToBounds = true
        imgView?.backgroundColor = UIColor.init(red: CGFloat(Double(red)/255.0), green: CGFloat(Double(green)/255.0), blue: CGFloat(Double(blue)/255.0), alpha: 1)
        
        titleLabel = UILabel(frame: .zero)
        titleLabel?.text = "item标题"
        titleLabel?.font = UIFont.systemFont(ofSize: 15)
        titleLabel?.textAlignment = .center
        self.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make) in
            make.left.right.equalTo(self)
            make.top.equalTo(imgView!.snp.bottom)
            make.height.equalTo(20)
        })
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
