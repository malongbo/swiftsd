//
//  TableViewCell.swift
//  ST
//
//  Created by Mac on 2020/9/2.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import SnapKit

class TableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var titleLabel:UILabel?
    var detailLabel:UILabel?
    var imgView:UIImageView?
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        imgView = UIImageView(frame: .zero)
        self.addSubview(imgView!)
        imgView?.snp.makeConstraints({ (make) in
            make.left.top.equalToSuperview().offset(8)
            make.height.width.equalTo(60)
        })
        
        let red = arc4random()%255
        let green = arc4random()%255
        let blue = arc4random()%255
        
        imgView?.layer.cornerRadius = 4
        imgView?.layer.masksToBounds = true
        imgView?.backgroundColor = UIColor.init(red: CGFloat(Double(red)/255.0), green: CGFloat(Double(green)/255.0), blue: CGFloat(Double(blue)/255.0), alpha: 1)
        
        titleLabel = UILabel(frame: .zero)
        titleLabel?.textAlignment = .justified
        titleLabel?.text = "我是大标题"
        titleLabel?.font = UIFont.systemFont(ofSize: 17)
        self.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo(imgView!)
            make.left.equalTo(imgView!.snp.right).offset(8)
            make.right.equalTo(-8)
            make.height.equalTo(20)
        })

        detailLabel = UILabel(frame: .zero)
        detailLabel?.textAlignment = .justified
        detailLabel?.numberOfLines = 0
        detailLabel?.font = UIFont.systemFont(ofSize: 12)
        self.addSubview(detailLabel!)
        detailLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo(titleLabel!.snp.bottom).offset(4)
            make.left.right.equalTo(titleLabel!)
//            make.height.equalTo(36)
            make.bottom.equalToSuperview().offset(-8)
        })

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    


}
