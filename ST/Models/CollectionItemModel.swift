//
//  CollectionItemModel.swift
//  ST
//
//  Created by Mac on 2020/9/3.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class CollectionItemModel: NSObject {

    var imgUrl:String = ""
    var title:String = ""
    var jumpUrl:String = ""
    
}
